<?php

namespace Drupal\jslog\Controller;

use donatj\UserAgent\UserAgentParser;
use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Html;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Returns response for Layout Info route.
 */
class LogController extends ControllerBase {

  /**
   * Maximum user agent string length. Used to validate invalid user agents.
   */
  const MAX_USER_AGENT_LENGTH = 200;

  /**
   * Maximum column number. Used to prevent attacks with huge numbers.
   */
  const MAX_COLNO = 1e10;

  /**
   * Maximum row number. Used to prevent attacks with huge numbers.
   */
  const MAX_LINENO = 1e10;

  /**
   * Maximum filename length. Used to prevent attacks with huge filenames.
   */
  const MAX_FILENAME_LENGTH = 200;

  /**
   * Maximum filename length. Used to prevent attacks with huge filenames.
   */
  const MAX_SCREEN_SIZE_LENGTH = 14;

  /**
   * Render the data as a message.
   *
   * @return string.
   */
  private function formatMessage(array $data): string {
    $message = [
      'message' => [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => Html::escape($data['message']),
      ],
    ];

    if (!$this->config('jslog.settings')->get('show_details')) {
      return \Drupal::service('renderer')->render($message);
    }

    $message['metadata'] = [
      '#type' => 'table',
      '#rows' => [],
    ];

    foreach ($data['metadata'] as $row) {
      if (!is_null($row['value'])) {
        $message['metadata']['#rows'][] = [
          [
            'data' => $this->t($row['label']),
            'header' => TRUE,
          ],
          [
            'data' => Html::escape($row['value']),
          ],
        ];
      }
    }

    return \Drupal::service('renderer')->render($message);
  }

  /**
   * Format request data to structured array.
   *
   * @return array.
   */
  private function formatData(array $request_data): array {
    $parser = new UserAgentParser();
    $ua = $parser->parse($request_data['user_agent']);

    return [
      'message' => substr($request_data['message'], 0, $this->config('jslog.settings')
        ->get('maximum_message_length')),
      'metadata' => [
        [
          'label' => 'Screen size',
          'value' => $request_data['screen_size'],
        ],
        [
          'label' => 'Filename',
          'value' => $request_data['filename'] ?? NULL,
        ],
        [
          'label' => 'Line:column',
          'value' => isset($request_data['lineno'], $request_data['colno']) ? "{$request_data['lineno']}:{$request_data['colno']}" : NULL,
        ],
        [
          'label' => 'Operating system',
          'value' => $ua->platform(),
        ],
        [
          'label' => 'Browser',
          'value' => $ua->browser() ? "{$ua->browser()} {$ua->browserVersion()}" : NULL,
        ],
        [
          'label' => 'User Agent',
          'value' => $request_data['user_agent'],
        ],
      ],
    ];
  }

  /**
   * Validate the request data.
   *
   * @return array
   *   An array which contains all errors.
   */
  public function validateData(array $request_data): array {
    $errors = [];
    $valid_keys = [
      'colno',
      'filename',
      'message',
      'lineno',
      'screen_size',
      'type',
      'user_agent',
    ];

    // Validate type.
    if (!isset($request_data['type'])) {
      $errors[] = $this->t('Type not set.');
    }
    else {
      if (!method_exists($this->getLogger('jslog'), $request_data['type'])) {
        $errors[] = $this->t('Invalid type @type.', ['@type' => $request_data['type']]);
      }
    }

    // Validate message.
    if (!isset($request_data['message'])) {
      $errors[] = $this->t('Message not set.');
    }

    // Validate user agent.
    if (!isset($request_data['user_agent'])) {
      $errors[] = $this->t('User agent not set.');
    }
    else {
      if (strlen($request_data['user_agent']) > self::MAX_USER_AGENT_LENGTH) {
        $errors[] = $this->t('Invalid user agent.');
      }
    }

    // Validate screen size.
    if (!isset($request_data['screen_size'])) {
      $errors[] = $this->t('Screen size not set.');
    }
    else {
      if (strlen($request_data['screen_size']) > self::MAX_SCREEN_SIZE_LENGTH) {
        $errors[] = $this->t('Invalid screen size length.');
      }
      else {
        if (!preg_match('/^\d+ 𝗑 \d+$/', $request_data['screen_size'])) {
          $errors[] = $this->t('Invalid screen size value.');
        }
      }
    }

    // Validate filename.
    if (isset($request_data['filename']) && strlen($request_data['filename']) > self::MAX_FILENAME_LENGTH) {
      $errors[] = $this->t('Invalid filename length.');
    }

    // Validate colno.
    if (isset($request_data['colno'])) {
      if (!is_int($request_data['colno'])) {
        $errors[] = $this->t('"colno" should be a number.');
      }
      else {
        if ($request_data['colno'] > self::MAX_COLNO) {
          $errors[] = $this->t('"colno" should be a valid number.');
        }
      }
    }

    // Validate lineno.
    if (isset($request_data['lineno'])) {
      if (!is_int($request_data['lineno'])) {
        $errors[] = $this->t('"lineno" should be a number.');
      }
      else {
        if ($request_data['lineno'] > self::MAX_LINENO) {
          $errors[] = $this->t('"lineno" should be a valid number.');
        }
      }
    }

    // Validate keys & values.
    foreach ($request_data as $key => $value) {
      if (!in_array($key, $valid_keys)) {
        $errors[] = $this->t('"@key" is an invalid key.', ['@key' => $key]);
      }
      else {
        if (!is_string($value) && !is_int($value)) {
          $errors[] = $this->t('Invalid value for "@key".', ['@key' => $key]);
        }
      }
    }

    return $errors;
  }

  /**
   * Logs a javascript message.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Error response or empty response.
   */
  public function log(Request $request): Response {
    $request_data = Json::decode($request->getContent());

    $errors = self::validateData($request_data);
    if (count($errors) !== 0) {
      return new Response(implode("\n", $errors), 422);
    }

    $data = self::formatData($request_data);
    $message = self::formatMessage($data);
    $this->getLogger('jslog')->{$request_data['type']}($message);

    return new Response('', 204);
  }

}
