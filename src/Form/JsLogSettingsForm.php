<?php

namespace Drupal\jslog\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for form used on quest detail page.
 *
 * @ingroup jslog
 */
class JsLogSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'jslog_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['jslog.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('jslog.settings');

    $form['excluded_messages'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Excluded messages'),
      '#description' => $this->t('List of excluded messages, separated by new lines.'),
      '#default_value' => $config->get('excluded_messages'),
    ];

    $form['excluded_user_agents_regex'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Excluded User Agents regex'),
      '#description' => $this->t('Exclude logs from certain user agents like crawlers or bots.'),
      '#default_value' => $config->get('excluded_user_agents_regex'),
    ];

    $form['maximum_messages_per_page'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum messages per page'),
      '#description' => $this->t('Limit the number of messages that are logged per page.'),
      '#default_value' => $config->get('maximum_messages_per_page'),
    ];

    $form['maximum_message_length'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum message length'),
      '#description' => $this->t('Limit the message length, mainly to prevent attacks with lot \'s of data.'),
      '#default_value' => $config->get('maximum_message_length'),
    ];

    $form['show_details'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show message details'),
      '#description' => $this->t('Show message details like screen width, browser, or OS.'),
      '#default_value' => $config->get('show_details'),
    ];

    return parent::buildForm($form, $form_state);
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('jslog.settings')
      ->set('excluded_messages', $form_state->getValue('excluded_messages'))
      ->set('excluded_user_agents_regex', $form_state->getValue('excluded_user_agents_regex'))
      ->set('maximum_messages_per_page', $form_state->getValue('maximum_messages_per_page'))
      ->set('show_details', $form_state->getValue('show_details'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
