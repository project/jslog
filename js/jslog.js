((Drupal, {path, excludedMessages, excludedUserAgents, maximumMessages, maximumMessageLength}) => {
  let messagesSent = 0
  if (new RegExp(excludedUserAgents).test(navigator.userAgent)) {
    return false
  }

  // Use a request queue to prevent DoS-ing the server in infinite loops
  let requestQueue = Promise.resolve()

  const log = (...messages) => {
    messages.filter(message => !excludedMessages.includes(message)).forEach(message => {
      if (messagesSent++ < maximumMessages) {
        sendMessage(message)
      }
      else if (messagesSent === maximumMessages) {
        sendMessage({
          message: Drupal.t('Maximum messages exceeded'),
          type: 'notice'
        })
      }
    })
  }

  const sendMessage = (message) => {
    if (message.message) {
      requestQueue = requestQueue.then(async () => {
        Object.assign(message, {
          message: message.message.substring(0, maximumMessageLength),
          user_agent: navigator.userAgent,
          screen_size: `${innerWidth} 𝗑 ${innerHeight}`,
        })
        dispatchEvent(new CustomEvent('jslog', {detail: message}))
        await fetch(path, {method: 'POST', body: JSON.stringify(message)})
      })
    }
  }

  // Store the original methods
  console.original = Object.assign({}, console);

  [
    {name: 'warn', type: 'warning'},
    {name: 'debug', type: 'debug'},
    {name: 'error', type: 'error'},
    {name: 'info', type: 'info'},
    {name: 'log', type: 'info'},
  ].forEach(({name, type}) => {
    console[name] = (...messages) => {
      console.original[name](...messages)
      log(...(messages.map(e => ({
        message: e.toString?.(),
        type,
      }))))
    }
  })

  addEventListener('error', ({message, filename, lineno, colno}) => {
    log({message, filename, lineno, colno, type: 'error'})
  })

  addEventListener('unhandledrejection', ({reason}) => {
    log({message: reason.stack || reason.message, type: 'notice'})
  })
})(Drupal, drupalSettings.jslog)
